package main

import (
	"strings"
	"testing"
)

func TestTask(t *testing.T) {
	sample := "OneTwoTrip"
	tests := [][]string{{"nOe1", "Two2", "Trip"}, {"nOe1", "Two2", "Tri3"}}

	for _, test := range tests {
		n := 0

		r, e := do(sample, test)
		if e != nil {
			t.Errorf("\nInput = %s\nError = %s", test, e)
			continue
		}

		for key, val := range r {
			if val.count != len(val.position) {
				t.Errorf("\nInput = %s\nError = %s", test,"Invalid position length")
				break
			}

			if strings.Index(strings.ToLower(sample), string(key)) < 0 {
				t.Errorf("\nInput = %s\nError = %s", test,"Invalid char in result list")
				break
			}

			n += len(val.position)
		}

		if n != len(sample) {
			t.Errorf("\nInput = %s\nError = %s", test, "Invalid count in position list")
		}
	}
}