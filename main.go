package main

import (
	"os"
	"fmt"
	"bufio"
	"errors"
	"unicode"
)

type charPosition struct {
	row int
	col int
	chr rune
}

type charPositionList struct {
	count    int
	position []charPosition
}

func main() {
	sample := "OneTwoTrip"

	matrix, err := readFile("input.txt")
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("INPUT:")
	for i := range matrix {
		fmt.Println(matrix[i])
	}

	result, err := do(sample, matrix)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("OUTPUT:")
	for _, chr := range sample {
		key := unicode.ToLower(chr)
		val := result[key]

		str := fmt.Sprintf("%c - (%d, %d);", val.position[0].chr, val.position[0].row, val.position[0].col)
		val.position = val.position[1:]
		fmt.Println(str)
	}
}

func readFile(path string) ([]string, error) {
	var data []string

	file, err := os.Open(path)
	if err != nil {
		return data, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		data = append(data, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		return data, err
	}

	return data[1:], nil
}

func do(sample string, matrix []string) (map[rune]*charPositionList, error) {
	flag := len(sample)
	data := make(map[rune]*charPositionList)

	for _, chr := range sample {
		key := unicode.ToLower(chr)

		if val, ok := data[key]; ok {
			val.count += 1
		} else {
			data[key] = &charPositionList{count: 1}
		}
	}

	for i := range matrix {
		for j, r := range matrix[i] {
			key := unicode.ToLower(r)

			if val, ok := data[key]; ok && (len(val.position) < val.count) {
				val.position = append(val.position, charPosition{row: i, col: j, chr: r})
				flag--

				if flag == 0 {
					return data, nil
				}
			}
		}
	}

	return nil, errors.New("Impossible")
}